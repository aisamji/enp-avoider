# An article in the Ismaili Insight newsletter
class Article
  attr_reader :title
  attr_reader :paragraphs

  def initialize(article_element)
    @title = article_element['title']
    @paragraphs = article_element.elements
  end
end
