require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class Florida
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, fort_lauderdale, miami, ocala, orlando, tampa]
  end

  def title
    'FLORIDA UNITED STATES'
  end

  def team
    'Florida Editorial Team'
  end

  def email
    'florida@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Florida Area',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'florida')
  end

  def fort_lauderdale
    Section.new('Fort Lauderdale',
                @regional_doc.at_xpath('//xmlns:fort-lauderdale'),
                'fort-lauderdale')
  end

  def miami
    Section.new('Miami', @regional_doc.at_xpath('//xmlns:miami'), 'miami')
  end

  def ocala
    Section.new('Ocala', @regional_doc.at_xpath('//xmlns:ocala'), 'ocala')
  end

  def orlando
    Section.new('Orlando', @regional_doc.at_xpath('//xmlns:orlando'), 'orlando')
  end

  def tampa
    Section.new('Tampa', @regional_doc.at_xpath('//xmlns:tampa'), 'tampa')
  end

  def locations
    [Location.new('Council for Florida',
                  ['12094 Miramar Parkway', 'Miramar, FL 33025'],
                  { 'Main' => '(954) 885-4010',
                    'Fax' => '(954) 885-4014' },
                  'lcf@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end

  def phone_numbers
    [
      PhoneNumber.new('Social Safety Net', '1 (866) SSN-HERE (776-4373)'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59'),
      PhoneNumber.new('National Economic Planning Board Helpline',
                      '1 (888) 6000-EPB (372)')
    ]
  end
end
