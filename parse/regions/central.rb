require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class Central
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, dallas, albuquerque, denver, little_rock,
                 oklahoma_city, tyler, waco]
  end

  def title
    'CENTRAL UNITED STATES'
  end

  def team
    'Central Editorial Team'
  end

  def email
    'central@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Central Region',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'central')
  end

  def dallas
    Section.new('Greater Dallas Area',
                @regional_doc.at_xpath('//xmlns:dallas'),
                'dallas',
                [
                  'Dallas Headquarters, Mid-Cities,',
                  'Plano, &amp; Tri-Cities'
                ])
  end

  def albuquerque
    Section.new('Albuquerque',
                @regional_doc.at_xpath('//xmlns:albuquerque'),
                'albuquerque')
  end

  def denver
    Section.new('Denver',
                @regional_doc.at_xpath('//xmlns:denver'),
                'denver')
  end

  def little_rock
    Section.new('Little Rock',
                @regional_doc.at_xpath('//xmlns:little-rock'),
                'little-rock')
  end

  def oklahoma_city
    Section.new('Oklahoma City',
                @regional_doc.at_xpath('//xmlns:oklahoma-city'),
                'oklahoma-city')
  end

  def tyler
    Section.new('Tyler',
                @regional_doc.at_xpath('//xmlns:tyler'),
                'tyler')
  end

  def waco
    Section.new('Waco',
                @regional_doc.at_xpath('//xmlns:waco'),
                'waco')
  end

  def locations
    [Location.new('Council for Central US',
                  ['1400 Ismaili Center Circle', 'Carrollton, TX 75006'],
                  { 'Main' => '(972) 446-5605 ext. 221',
                    'Fax' => '(972) 446-8233' },
                  'lcc@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end

  def phone_numbers
    [
      PhoneNumber.new('ACCESS', '1 (844) 55-ACCESS (2-2237)'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59')
    ]
  end
end
