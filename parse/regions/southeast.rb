require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class Southeast
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, atlanta, birmingham, tennesse, spartanburg]
  end

  def title
    'SOUTHEASTERN UNITED STATES'
  end

  def team
    'Southeastern Editorial Team'
  end

  def email
    'southeastern@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Southeastern Region',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'southeast')
  end

  def atlanta
    Section.new('Greater Atlanta Area',
                @regional_doc.at_xpath('//xmlns:atlanta'),
                'atlanta',
                ['Atlanta Headquarters, Atlanta Northeast, Atlanta Northwest,',
                 'Atlanta South &amp; Duluth'])
  end

  def birmingham
    Section.new('Birmingham',
                @regional_doc.at_xpath('//xmlns:birmingham'),
                'birmingham')
  end

  def tennessee
    Section.new('Tennessee',
                @regional_doc.at_xpath('//xmlns:tennessee'),
                'tennessee',
                ['Nashville, Chattanooga, Memphis, Knoxville'])
  end

  def spartanburg
    Section.new('Spartanburg',
                @regional_doc.at_xpath('//xmlns:spartanburg'),
                'spartanburg')
  end

  def locations
    [Location.new('Council for Southeast',
                  ['685 DeKalb Industrial Way', 'Decatur, GA 30033'],
                  { 'Main' => '(404) 299-5760',
                    'Fax' => '(866) 879-6486' },
                  'lcse@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end

  def phone_numbers
    [
      PhoneNumber.new('ACCESS Help Desk', '1 (844) 552 2237'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59'),
      PhoneNumber.new('National Economic Planning Board Helpline',
                      '1 (888) 6000-EPB (372)')
    ]
  end
end
