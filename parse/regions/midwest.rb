require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class Midwest
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, chicago, cleveland, detroit_lansing, kansas_city,
                 milwaukee, minneapolis]
  end

  def title
    'MIDWESTERN UNITED STATES'
  end

  def team
    'Midwestern Editorial Team'
  end

  def email
    'midwestern@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Midwestern Region',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'midwest')
  end

  def chicago
    Section.new('Greater Chicago Area',
                @regional_doc.at_xpath('//xmlns:chicago'),
                'chicago',
                ['Chicago Downtown, Glenview Headquarters, &amp; Naperville'])
  end

  def cleveland
    Section.new('Cleveland',
                @regional_doc.at_xpath('//xmlns:cleveland'),
                'cleveland')
  end

  def detroit_lansing
    Section.new('Detroit &amp; Lansing',
                @regional_doc.at_xpath('//xmlns:detroit-lansing'),
                'detroit-lansing')
  end

  def kansas_city
    Section.new('Kansas City',
                @regional_doc.at_xpath('//xmlns:kansas-city'),
                'kansas-city')
  end

  def milwaukee
    Section.new('Milwaukee',
                @regional_doc.at_xpath('//xmlns:milwaukee'),
                'milwaukee')
  end

  def minneapolis
    Section.new('Minneapolis',
                @regional_doc.at_xpath('//xmlns:minneapolis'),
                'minneapolis')
  end

  def locations
    [Location.new('Council for Midwest',
                  ['100 Shermer Road',
                   'Glenview, IL 60025'],
                  { 'Main' => '(847) 730-1950' },
                  'lcmw@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end

  def phone_numbers
    [
      PhoneNumber.new('Social Safety Net', '1 (866) SSN-HERE (776-4373)'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59'),
      PhoneNumber.new('National Economic Planning Board Helpline',
                      '1 (888) 6000-EPB (372)')
    ]
  end
end
