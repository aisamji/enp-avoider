require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class Southwest
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, houston, central_texas]
  end

  def title
    'SOUTHWESTERN UNITED STATES'
  end

  def team
    'Southwestern Editorial Team'
  end

  def email
    'southwestern@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Southwestern Region',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'southwest')
  end

  def houston
    Section.new('Greater Houston Area',
                @regional_doc.at_xpath('//xmlns:houston'),
                'houston',
                ['Beaumont, Clear Lake, Houston Headquarters, Principal',
                 ' Jamatkhana, Houston North, Katy, Sugar Land'])
  end

  def central_texas
    Section.new('Central Texas Area',
                @regional_doc.at_xpath('//xmlns:central-texas'),
                'central-texas',
                ['Austin, College Station, Corpus Christi, San Antonio'])
  end

  def locations
    [Location.new('Council for Southwest',
                  ['11111 Brooklet Drive Suite 210',
                   'Houston, TX 77099'],
                  { 'Main' => '(281) 568-0218',
                    'Fax' => '(281) 568-8130' },
                  'lcsw@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end

  def phone_numbers
    [
      PhoneNumber.new('Social Safety Net', '1 (866) SSN-HERE (776-4373)'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59'),
      PhoneNumber.new('National Economic Planning Board Helpline',
                      '1 (888) 6000-EPB (372)')
    ]
  end
end
