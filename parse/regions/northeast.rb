require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class Northeast
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, boston, edison, new_york, pennyslvania,
                 richmond, washington_dc]
  end

  def title
    'NORTHEASTERN UNITED STATES'
  end

  def team
    'Northeastern Editorial Team'
  end

  def email
    'northeastern@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Northeastern Region',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'northeast')
  end

  def boston
    Section.new('Boston', @regional_doc.at_xpath('//xmlns:boston'), 'boston')
  end

  def edison
    Section.new('Edison', @regional_doc.at_xpath('//xmlns:edison'), 'edison')
  end

  def new_york
    Section.new('Greater New York Area',
                @regional_doc.at_xpath('//xmlns:new-york'),
                'new-york',
                ['Albany, Lake Success, Manhattan, New York Headquarters,',
                 'Poughkeepsie, &amp; Westport'])
  end

  def pennsylvania
    Section.new('Greater Pennsylvania Area',
                @regional_doc.at_xpath('//xmlns:pennsylvania'),
                'pennsylvania',
                ['Philadelphia &amp; Lancaster'])
  end

  def richmond
    Section.new('Richmond',
                @regional_doc.at_xpath('//xmlns:richmond'),
                'richmond')
  end

  def washington_dc
    Section.new('Washington, DC',
                @regional_doc.at_xpath('//xmlns:washington-dc'),
                'washington-dc')
  end

  def locations
    [Location.new('Council for Northeast',
                  ['92-68 Queens Boulevard', 'Rego Park, NY 11374'],
                  { 'Main' => '(718) 478-5594 ext. 201',
                    'Fax' => '(718) 426-8314' },
                  'lcne@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end

  def phone_numbers
    [
      PhoneNumber.new('Social Safety Net', '1 (866) SSN-HERE (776-4373)'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59'),
      PhoneNumber.new('National Economic Planning Board Helpline',
                      '1 (888) 6000-EPB (372)')
    ]
  end
end
