require_relative '../section'
require_relative '../location'
require_relative '../phone_number'

# Region specific information for the Central region
class West
  attr_reader :sections

  def initialize(regional_doc)
    @regional_doc = regional_doc
    @sections = [everywhere, los_angeles, nor_cal, cen_cal, las_vegas,
                 phoenix, portland, san_diego, seattle]
  end

  def title
    'WESTERN UNITED STATES'
  end

  def team
    'Western Editorial Team'
  end

  def email
    'western@IsmailiInsight.org'
  end

  def everywhere
    Section.new('Western Region',
                @regional_doc.at_xpath('//xmlns:everywhere'),
                'west')
  end

  def los_angeles
    Section.new('Greater Los Angeles Area',
                @regional_doc.at_xpath('//xmlns:los-angeles'),
                'los-angeles',
                ['Los Angeles Headquarters, San Fernando Valley, Torrance,',
                 'Orange County, San Gabriel Valley, &amp; Inland Empire'])
  end

  def nor_cal
    Section.new('Northern California Area',
                @regional_doc.at_xpath('//xmlns:nor-cal'),
                'nor-cal',
                ['Santa Clara Valley, Alameda, Marin County, &amp; Sacramento'])
  end

  def cen_cal
    Section.new('Central California Area',
                @regional_doc.at_xpath('//xmlns:cen-cal'),
                'cen-cal',
                ['Bakersfield &amp; Visalia'])
  end

  def las_vegas
    Section.new('Las Vegas',
                @regional_doc.at_xpath('//xmlns:las-vegas'),
                'las-vegas')
  end

  def phoenix
    Section.new('Phoenix', @regional_doc.at_xpath('//xmlns:phoenix'), 'phoenix')
  end

  def portland
    Section.new('Portland',
                @regional_doc.at_xpath('//xmlns:portland'),
                'portland')
  end

  def san_diego
    Section.new('San Diego',
                @regional_doc.at_xpath('//xmlns:san-diego'),
                'san-diego')
  end

  def seattle
    Section.new('Seattle', @regional_doc.at_xpath('//xmlns:seattle'), 'seattle')
  end

  # rubocop:disable Metrics/MethodLength
  def locations
    [Location.new('Council for West',
                  ['3350 Ocean Park Boulevard',
                   'Suite 210',
                   'Santa Monica, CA 90405'],
                  { 'Main' => '(310) 214-5024',
                    'Fax' => '(310) 214-5034' },
                  'lcsw@usaji.org'),
     Location.new('Aga Khan Foundation USA',
                  ['1825 K Street NW, Suite 901', 'Washington, DC 20006'],
                  { 'Main' => '(202) 293-2537' },
                  'info.akfusa@akdn.org')]
  end
  # rubocop:enable Metrics/MethodLength

  def phone_numbers
    [
      PhoneNumber.new('Social Safety Net', '1 (866) SSN-HERE (776-4373)'),
      PhoneNumber.new('FOCUS USA', '1 (877) FOCUS 59'),
      PhoneNumber.new('National Economic Planning Board Helpline',
                      '1 (888) 6000-EPB (372)')
    ]
  end
end
