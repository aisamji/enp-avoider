require_relative 'article'

# A section in the Ismaili Insight newsletter
class Section
  attr_reader :title
  attr_reader :subsections
  attr_reader :short_name
  attr_reader :articles

  def initialize(title, section_element, short_name, subsections = [])
    @title = title
    @subsections = subsections.join('<br>')
    @short_name = short_name
    @articles =
      section_element.xpath('./xmlns:article').map do |art_elem|
        Article.new(art_elem)
      end
  end
end
