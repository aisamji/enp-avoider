# An important location for the Ismaili community
class Location
  attr_reader :title
  attr_reader :address
  attr_reader :email
  attr_reader :phones

  def initialize(title, address_lines, phone_numbers, email)
    @title = title
    @address = address_lines.join('<br>')
    @email = email
    @phones = phone_numbers.map { |t, n| "#{t}: #{n}" }
  end
end
