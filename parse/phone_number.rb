# Important phone numbers for the Ismaili community
class PhoneNumber
  attr_reader :name
  attr_reader :number

  def initialize(name, number)
    @name = name
    @number = number
  end
end
