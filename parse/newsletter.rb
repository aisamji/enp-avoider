require 'nokogiri'
require_relative 'section'
require_relative 'website'

# A newsletter for the Ismaili Insight
class Newsletter
  def initialize(national_xml, regional_xml)
    @national_xml = national_xml
    @regional_xml = regional_xml
    refresh_national
    refresh_region
  end

  def refresh_national
    national_doc = File.open(@national_xml) { |f| Nokogiri::XML(f) }
    @chaandraat = national_doc.at_xpath('//xmlns:header/xmlns:chaandraat').text
    @date = national_doc.at_xpath('//xmlns:header/xmlns:date').text
    @images = national_doc.at_xpath('//xmlns:header/xmlns:images')
    @events = Section.new('', national_doc.at_xpath('//xmlns:events'),
                          'national')
    @news = Section.new('', national_doc.at_xpath('//xmlns:news'), 'news')
  end

  def refresh_region
    regional_doc = File.open(@regional_xml) { |f| Nokogiri::XML(f) }
    region_name = regional_doc.root.name
    require_relative "regions/#{region_name}"
    # rubocop:disable Security/Eval
    eval("@region = #{region_name.capitalize}.new(regional_doc)")
  end

  # National Information
  attr_reader :chaandraat
  attr_reader :date
  attr_reader :images
  attr_reader :events
  attr_reader :news

  # rubocop:disable Metrics/MethodLength
  def websites
    [
      [Website.new('TheIsmaili.org', 'http://www.theismaili.org/'),
       Website.new('AKDN.org', 'http://www.akdn.org/'),
       Website.new('IIS.ac.uk', 'http://www.iis.ac.uk/')],

      [Website.new('EduOnline.net', 'https://www.eduonline.ne/home/login.aspx'),
       Website.new('FinOnline.net', 'http://www.finonline.net/logi/login.aspx'),
       Website.new('IPNOnline.net', 'http://ipnonline.net/')],

      [Website.new('IHPAOnline.net', 'http://ihpaonline.org/'),
       Website.new('AKFUSA.org', 'http://www.focus-usa.org/'),
       Website.new('FOCUS', 'http://www.akdn.org/focus')]
    ]
  end
  # rubocop:enable Metrics/MethodLength

  # Regional Information
  def sections
    @region.sections
  end

  def title
    @region.title
  end

  def team
    @region.team
  end

  def email
    @region.email
  end

  def locations
    @region.locations
  end

  def phone_numbers
    @region.phone_numbers
  end

  # Allow ERB binding
  def my_binding
    binding
  end
end
