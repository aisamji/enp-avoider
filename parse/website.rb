# An important website for the ismaili community
class Website
  attr_reader :name
  attr_reader :url

  def initialize(name, url)
    @name = name
    @url = url
  end
end
