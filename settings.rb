# Common settings
module Settings
  # Common Colors
  module Colors
    def self.orange
      '#ff9900'
    end

    def self.white
      '#ffffff'
    end

    def self.gray
      '#595959'
    end
  end

  # Font settings
  module Font
    def self.family
      'segoe ui'
    end

    # Common Font sizes
    module Sizes
      # Help & Information
      def self.newsletter_help
        '15px'
      end

      def self.newsletter_help_small
        '14px'
      end

      def self.contact_info
        '11px'
      end

      def self.resource_header
        '16px'
      end

      def self.resource
        '13px'
      end

      # Content Headers
      def self.newsletter_header
        '17px'
      end

      def self.category_header
        '24px'
      end

      def self.section_header
        '16px'
      end

      def self.article_header
        '16px'
      end

      # Table of Contents
      def self.category_toc_item
        '14px'
      end

      def self.toc_item
        '12px'
      end

      def self.toc_subtitle_item
        '11px'
      end

      # Articles
      def self.article_body
        '13px'
      end

      def self.image_caption
        '10px'
      end

      def self.return_top
        '10px'
      end

      # Aesthetic Table
      def self.table_header
        '16px'
      end

      def self.table_data
        '11px'
      end
    end
  end
end
