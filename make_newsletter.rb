require 'erb'
require_relative 'parse/newsletter'

file = File.open('newsletter.html.erb')
template = file.read
file.close

raise 'Hell' if ARGV.length < 2
national_file = ARGV[0]
regional_file = ARGV[1]

newsletter = Newsletter.new(national_file, regional_file)

renderer = ERB.new(template)
puts renderer.result(newsletter.my_binding)
